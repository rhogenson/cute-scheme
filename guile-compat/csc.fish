#!/usr/bin/env nix-shell
#! nix-shell -i fish -p fish guile_3_0 rlwrap
rlwrap guile -l (dirname (status --current-filename))/compat.scm $argv
