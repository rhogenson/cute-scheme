(define-library (csc ir)
  (export
    *void*
    apply-args
    apply-func
    apply?
    call-builtin-args
    call-builtin-name
    call-builtin?
    const-val
    const?
    label-var
    label?
    lambda-body
    lambda-vars
    lambda?
    letrec-body
    letrec-funcs
    letrec?
    libvar-lib
    libvar-var
    libvar?
    make-apply
    make-call-builtin
    make-const
    make-label
    make-lambda
    make-letrec
    make-libvar
    make-primop
    make-sequence
    make-set
    primop-args
    primop-ks
    primop-name
    primop-vals
    primop?
    sequence-head
    sequence-tail
    sequence?
    set-body
    set-var
    set?
    void?)
  (import (scheme base))
  (begin


    (define-record-type <libvar>
      (make-libvar library var)
      libvar?
      (library libvar-lib)
      (var libvar-var))


    ;; ----------  IR1 

    (define-record-type <lambda>
      (make-lambda vars body)
      lambda?
      (vars lambda-vars)
      (body lambda-body))


    (define-record-type <letrec>
      (make-letrec funcs body)
      letrec?
      (funcs letrec-funcs)
      (body letrec-body))


    (define-record-type <apply>
      (make-apply func args)
      apply?
      (func apply-func)
      (args apply-args))


    (define-record-type <sequence>
      (make-sequence head tail)
      sequence?
      (head sequence-head)
      (tail sequence-tail))


    (define-record-type <set>
      (make-set var body)
      set?
      (var set-var)
      (body set-body))


    (define-record-type <call-builtin>
      (make-call-builtin name args)
      call-builtin?
      (name call-builtin-name)
      (args call-builtin-args))


    (define-record-type <const>
      (make-const val)
      const?
      (val const-val))


    (define-record-type <void>
      (make-void)
      void?)


    (define *void* (make-void))


    ;; -------------  CPS

    ; <apply>, <letrec>, <const>, and <void> are also IR2 forms.


    (define-record-type <label>
      (make-label var)
      label?
      (var label-var))


    (define-record-type <primop>
      (make-primop name arguments values continuations)
      primop?
      (name primop-name)
      (arguments primop-args)
      (values primop-vals)
      (continuations primop-ks))))
