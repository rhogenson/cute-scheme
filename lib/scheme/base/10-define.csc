(export
  define
  define-syntax
  set!)
(import (only (csc builtins)
          builtin-define
          define-syntax
          lambda
          set!
          syntax-rules))
(begin


  (define-syntax define
    (syntax-rules ()
      ((define (f . args) body ...)
        (define f (lambda args body ...)))
      ((define name binding)
        (builtin-define name binding)))))
