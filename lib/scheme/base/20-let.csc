(export
  begin
  lambda
  let
  let*
  letrec
  letrec*)
(import (only (csc builtins)
          builtin-sequence
          define-syntax
          lambda))
(begin


  (define-syntax letrec*
    (syntax-rules ()
      ((letrec* ((var1 init1) ...) body1 body2 ...)
        (lambda ()
          (define var1 init1)
          ...
          body1 body2 ...))))


  (define-syntax letrec-generate-temp-names
    (syntax-rules ()
      ((letrec-generate-temp-names
         ()
         (temp1 ...)
         ((var1 init1) ...)
         body ...)
        (letrec* ((temp1 init1)
                  ...
                  (var1 temp1)
                  ...)
          body ...))
      ((letrec-generate-temp-names
         (x y ...)
         (temp ...)
         ((var1 init1) ...)
         body ...)
        (letrec-generate-temp-names
          (y ...)
          (newtemp temp ...)
          ((var1 init1) ...)
          body ...))))


  (define-syntax letrec
    (syntax-rules ()
      ((letrec ((var1 init1) ...) body1 body2 ...)
        (letrec-generate-temp-names
          (var1 ...)
          ()
          ((var1 init1) ...)
          body1 body2 ...))))


  (define-syntax let
    (syntax-rules ()
      ((let ((name val) ...) body1 body2 ...)
        ((lambda (name ...) body1 body2 ...)
         val ...))
      ((let tag ((name val) ...) body1 body2 ...)
        ((letrec ((tag (lambda (name ...)
                         body1 body2 ...)))
           tag)
         val ...))))


  (define-syntax let*
    (syntax-rules ()
      ((let* () body1 body2 ...)
        (let () body1 body2 ...))
      ((let* ((name1 val1) (name2 val2) ...)
         body1 body2 ...)
        (let ((name1 val1))
          (let* ((name2 val2) ...)
            body1 body2 ...)))))


  (define-syntax begin
    (syntax-rules ()
      ((begin)
        #f)
      ((begin expr)
        expr)
      ((begin body1 body2 ...)
        (builtin-sequence body1 (begin body2 ...))))))
