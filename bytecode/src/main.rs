mod bytecode;
mod data;
mod encoding;
mod heap;

use std::fs::File;

fn usage() {
    println!("Usage: bytecode <file>");
}

fn main() {
    let filepath = match std::env::args().nth(1) {
        Some(x) => x,
        None => {
            usage();
            std::process::exit(255);
        }
    };
    let prog = {
        let mut file = match File::open(filepath) {
            Ok(x) => x,
            Err(err) => {
                println!("Can't open file for reading: {}", err);
                std::process::exit(255);
            }
        };
        match encoding::decode(&mut file) {
            Ok(x) => x,
            Err(err) => {
                println!("Invalid bytecode: {}", err);
                std::process::exit(255);
            }
        }
    };
    let exit_code = match bytecode::eval(&prog) {
        Ok(x) => x,
        Err(err) => {
            println!("{}", err);
            std::process::exit(255);
        }
    };
    std::process::exit(i32::from(exit_code));
}
