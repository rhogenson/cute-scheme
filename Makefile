CSC := guile-compat/csc.fish

.PHONY: test-csc
test: *.csc
	$(CSC) tests/test-main.csc lib/csc/*-test.csc

.PHONY: test
test: test-csc
